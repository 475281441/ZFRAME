<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9
 * Time: 10:04
 */

namespace zframe\system;

use zframe\library\File;

class Log
{

    const LOG = 'log';

    protected static $instance = null;

    protected static $today_path;//今日缓存目录
    protected static $log_root_path = 'logs';//runtime目录下的子目录

    protected static $max_log = 2;//Mb
    protected static $log_prefix = '.log';//日志文件后缀

    protected static $new_log_format = 'Hi_s';//1738_55

    protected static $time;

    protected static $content = [];//需要写的日志

    public function __construct()
    {
        self::$time = time();
        $time = date('Ymd', self::$time);
        $path_1 = substr($time, 0, 4);
        $path_2 = substr($time, 4, 2);
        self::$today_path = $path_1 . DS . $path_2;
        File::Directory(RUNTIME_PATH . DS . self::$log_root_path . DS . self::$today_path);//检测今日缓存目录，不存在就创建
    }

    /**
     * 获取实例
     * @return null|Log
     */
    public function getInstance()
    {
        if (null != self::$instance) {
            return self::$instance;
        } else {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * 普通日志
     * @param $log
     */
    public static function log($log)
    {
        return self::record($log, 'log');
    }

    /**
     * 错误日志
     * @param $log
     */
    public static function error($log)
    {
        return self::record($log, 'error');
    }

    /**
     * 提示日志
     * @param $log
     */
    public static function notice($log)
    {
        return self::record($log, 'notice');
    }

    /**
     * 记录日志
     * @param $log
     * @param string $tag
     */
    private static function record($log, $tag = 'DEBUG')
    {
        $tag = mb_strtoupper($tag);
        self::$content[] = "\r\n[" . $tag . ']';
        self::$content[] = '[' . date('Y-m-d H:i:s', self::$time) . ' ' . date_default_timezone_get() . ' ]';
        $log_array = explode("\r\n", $log);
        foreach ($log_array as $v) {
            self::$content[] = $v;
        }

    }

    /**
     * 写日志文件
     * @throws \Exception
     */
    public static function write()
    {
        $file_name = date('d', self::$time) . self::$log_prefix;

        $new_file_name = date('d', self::$time) . '_' . date(self::$new_log_format, self::$time) . self::$log_prefix;

        $full_path = RUNTIME_PATH . DS . self::$log_root_path . DS . self::$today_path;

        if (is_dir($full_path)) {
            @$log_size = filesize($full_path . DS . $file_name);
            if ($log_size >= self::$max_log * 1000000) {
                //拆分文件
                File::Move($full_path . DS . $file_name, $full_path . DS . $new_file_name);
            }
            array_push(self::$content, '------------------------------------------------');
            self::$content = implode("\r\n", self::$content);
            error_log(self::$content, 3, $full_path . DS . $file_name);

        } else {
            throw new \Exception('Log path is not visible');
        }
    }

}