<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/10
 * Time: 11:20
 */

namespace zframe\system\facade;

/**
 * Class Log
 * @package zframe\system\facade
 *
 * @method void log($msg) static 记录普通日志
 * @method void error($msg) static
 * @method void notice($msg) static
 * @method void write() static
 */
class Log
{
    protected static $type = ['log', 'error', 'notice', 'write'];

    public static function __callStatic($name, $arguments)
    {
        if (in_array($name, self::$type)) {
            $object = new \zframe\system\Log();
            $instance = $object->getInstance();
            return $instance::{$name}(isset($arguments[0]) ? $arguments[0] : '');
        }
    }

}