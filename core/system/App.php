<?php
/**
 * APP请求调用
 * User: Administrator
 * Date: 2018/7/3
 * Time: 17:41
 */

namespace zframe\system;

class App
{

    protected static $instance = null;

    protected static $request = null;

    protected static $module;
    protected static $controller;
    protected static $action;


    public function __construct()
    {
        date_default_timezone_set('Asia/Shanghai');
    }

    /**
     *启动app
     */
    public static function start()
    {
        self::$request = Request::instance();
        self::$module = self::$request->module();
        self::$controller = self::$request->controller();
        self::$action = self::$request->action();
        self::call();
    }


    /**
     * 请求指定action
     * @throws \ReflectionException
     */
    public static function call()
    {
        $class = APP . '\\' . self::$module . '\controller\\' . self::$controller;
        $reflat = new \ReflectionClass($class);
        if ($reflat->hasMethod(self::$action)) {
            $method = new \ReflectionMethod($class, self::$action);
            if ($method->isPublic()) {//控制器中的函数必须为public或默认，才可被外部访问
                return $reflat->newInstance()->{self::$action}();
//                return call_user_func([$reflat->newInstance(), self::$action]);
            } else {
                throw new \Exception('method ' . self::$action . ' is not public');
            }

        } else {
            throw new \Exception('method ' . self::$action . ' is not exists');
        }

    }

    public static function end(){
        \zframe\system\facade\Log::write();
    }
}
