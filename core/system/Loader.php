<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/3
 * Time: 12:02
 */

namespace zframe\system;

class Loader
{
    private static $sys_namespace_map = [];//系统类库命名空间映射

    private static $app_namespace_map = [];//app命名空间映射

    static $app_path = APP;

    /**
     * 注册自动加载
     * @param string $Loader 自定义loader，默认使用框架自带loader进行加载
     * @throws \Exception
     */
    public static function Register($Loader = '')
    {
        spl_autoload_register($Loader ? $Loader : array('self', 'autoload'), true, true);

        //进行命名空间映射
        self::$sys_namespace_map = [
            'zframe/library'       => CORE_PATH . 'library',
            'zframe/system'        => CORE_PATH . 'system',
            'zframe/system/facade' => CORE_PATH . 'system' . DS . 'facade',
        ];

        self::load_app_namespace('');
    }

    /**
     * 自动加载实现
     * @param $class
     */
    public static function autoload($class)
    {
        $class = str_replace('\\', '/', $class);//命名空间统一将\转换为/
        __include_file(self::findFile($class));
    }

    /**
     * 查找类库文件
     * @param $class
     * @return string
     */
    public static function findFile($class)
    {
        if (isset(self::$sys_namespace_map[dirname($class)])) {//加载系统类库
            return self::$sys_namespace_map[dirname($class)] . DS . basename($class) . EXT;
        } elseif (isset(self::$app_namespace_map[dirname(($class))])) {//加载app目录
            return self::$app_namespace_map[dirname($class)] . DS . basename($class) . EXT;
        }
    }

    /**
     * 加载app命名空间
     * @param string $path
     * @throws \Exception
     */
    public static function load_app_namespace($path = '')
    {
        $app_path    = $path ? $path : self::$app_path;
        $module_path = ROOT_PATH . DS . $app_path;
        $dirs        = [];

        if (function_exists('scandir')) {
            $dirs = scandir($module_path);
        } else {
            throw new \Exception('function scandir is disabled..');
        }

        if (count($dirs) > 2) {

            foreach ($dirs as $k => $v) {
                if ($v <> '.' && $v <> '..') {
                    self::$app_namespace_map[$app_path . '/' . $v . '/controller'] = $module_path . DS . $v . DS . 'controller' . DS;
                }
            }
        }

    }

}

function __include_file($file)
{
    return include $file;
}