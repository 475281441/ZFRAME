<?php
/**
 * 文件处理类
 * User: Administrator
 * Date: 2018/7/9
 * Time: 17:07
 */

namespace zframe\library;


class File
{
    /**
     * 检测目录
     * @param $dir
     * @return bool
     */
    public static function Directory($dir)
    {
        return is_dir($dir) or self::Directory(dirname($dir)) and mkdir($dir, 0777);
    }

    /**
     * 移动文件
     * @param $old
     * @param $new
     * @return bool|string
     */
    public static function Move($old, $new)
    {
        if (!file_exists($old)) {
            return 'old file not exist';
        }
        $new_dir = dirname($new);

        if (!is_dir($new_dir)) {
            self::Directory($new_dir);
        }

        return rename($old, $new);

    }
}