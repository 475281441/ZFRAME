<?php
/**
 * 框架入口文件
 * User: Administrator
 * Date: 2018/7/2
 * Time: 18:20
 */

namespace zframe;

use zframe\system\App;
use zframe\system\Loader;

define('ZFRAME_VERSION', '1.00');
define('ZF_START_TIME', microtime(true));
define('ZF_START_MEM', memory_get_usage());
define('EXT', '.php');
define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') or define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']) . DS);//部署路径
defined('ROOT_PATH') or define('ROOT_PATH', dirname(realpath(APP_PATH)) . DS);
defined('RUNTIME_PATH') or define('RUNTIME_PATH', ROOT_PATH . 'runtime' . DS);
define('CORE_PATH', ROOT_PATH . 'core' . DS);

// 环境常量
define('IS_CLI', PHP_SAPI == 'cli' ? true : false);
define('IS_WIN', strpos(PHP_OS, 'WIN') !== false);

defined('APP') or define('APP', 'app');//定义app目录

require CORE_PATH . 'system' . DS . 'Loader.php';
require_once ROOT_PATH . DS . 'vendor' . DS . 'autoload.php';
Loader::Register();//注册自动加载
App::start();//开始